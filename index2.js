// bai 1
/**
 * dau vao : luong hang ngay, so ngay lam viec
 * cac buoc xu ly:
 * 1. Tao hang so luong hang ngay salary va gan gia tri 100000 
 * 2. Tao bien so ngay lam viec day va gan gia tri 22
 * 3. tinh tong luong trong thang bang totalSalary = salary *day
 * 4. in ket qua ra console
 * 
 * dau ra: tong luong trong thang
 */
function tinh1(){

var salary = document.getElementById("salary").value *1;
var day = document.getElementById("day").value *1;
var totalSalary = salary * day;
document.getElementById("result-1").innerHTML=totalSalary;
event.preventDefault();
console.log("🚀 ~ file: index.js ~ line 18 ~ tinh ~ totalSalary", totalSalary)
}
// bai 2
/**
 * dau vao : 5 so thuc
 * cac buoc xu ly:
 * 1. Tao bien cho 5 so thuc va gan gia tri
 * num1 =2, num2=3, num3=5.5, num4=10/3, num5 = -9
 * 2. tinh trung binh cong cua 5 so thuc avg= (num1 + num2 +num3+ num4+num5)/5;
 * 3. in ket qua ra console
 * 
 * dau ra: gia tri trung binh cua 5 so thuc
 */

function tinh2(){
var num1 = document.getElementById("num1").value*1;
var num2 = document.getElementById("num2").value*1;
var num3 = document.getElementById("num3").value*1;
var num4 = document.getElementById("num4").value*1;
var num5 = document.getElementById("num5").value*1;
var avg= (num1 + num2 +num3+ num4+num5)/5;

document.getElementById("result-2").innerHTML=avg;
event.preventDefault();
}
// bai 3
/**
 * dau vao : don gia USD, so tien USD can quy doi thanh VND
 * cac buoc xu ly:
 * 1. Tao hang so va gan gia tri cho don gia dong USD donGiaUSD =23500
 * 2. Tao bien va gan gia tri cho so tien USD can quy doi USDamt = 80
 * 3. Tao bien va gan gia tri cho so tien VND quy doi bang cong thuc VNDamt = USDamt * donGiaUSD
 * 4. in ket qua ra console
 * 
 * dau ra: so tien VND quy doi
 */
 function tinh3(){

const donGiaUSD = 23500;
var USDamt = document.getElementById("USD-amount").value*1;
console.log("🚀 ~ file: index.js ~ line 59 ~ tinh3 ~ USDamt", USDamt)
var VNDamt = USDamt * donGiaUSD;
console.log("🚀 ~ file: index.js ~ line 61 ~ tinh3 ~ donGiaUSD", donGiaUSD)
console.log("🚀 ~ file: index.js ~ line 61 ~ tinh3 ~ VNDamt", VNDamt)
document.getElementById("result-3").innerHTML=VNDamt;
event.preventDefault();
 }
// bai 4
/**
 * dau vao : chieu dai va chieu rong
 * cac buoc xu ly:
 * 1. tao bien va gan gia tri cho chieu lenght =20, width=70
 * 2. tao bien va gan gia tri cho Dien tich theo cong thuc S = lenght*width
 * 3. tao bien va gan gia tri cho chu vi theo cong thuc  D = (lenght+width)*2;
 * 4. in ket qua chu vi va dien tich ra console 
 * 
 * dau ra: chu vi va dien tich
 */

function tinh4(){
var lenght =document.getElementById("lenght").value*1;
var width = document.getElementById("width").value*1;

var s = lenght*width;
var d = (lenght+width)*2;
document.getElementById("result-4-s").innerHTML=s;
document.getElementById("result-4-d").innerHTML=d;
event.preventDefault();
}





// bai 5
/**
 * dau vao : số có 2 chữ số
 * cac buoc xu ly:
 * 1. tao bien va gan gia tri cho so co 2 chu so
 * 2. tao bien va gan gia tri cho so hang chu theo cong thuc soHangChuc = Math.floor(Digit/10)
 * 3. tao bien va gan gia tri cho so hang don vi theo cong thuc  soHangDonVi= Digit%10
 * 4. tao bien va gan gia tri tong 2 ky so theo cong thuc sum = soHangChuc +soHangDonVi
 * 5. in ket qua ra console 
 * 
 * dau ra: tong 2 ky so
 */
function tinh5(){
var digit = document.getElementById("digit").value*1;

var soHangChuc = Math.floor(digit/10);
var soHangDonVi= digit%10;

var sum = soHangChuc +soHangDonVi;
document.getElementById("result-5").innerHTML=sum;
event.preventDefault();
}